from django.urls import path
from spotEr import views

urlpatterns = [
    path('', views.index, name='home'),
    # authentications
    path('signup', views.sign_up, name='signup'),
    path('login', views.user_login, name='login'),
    path('logout', views.user_logout, name='logout'),
    # Add friend
    path('addFriend/<int:id>/', views.add_friend, name='addFriend'),
    # Connections
    path('myConnections', views.my_connections, name='myConnections'),
    #export xlsx
    path('export_users_xls', views.export_users_xls, name='export_users_xls')

]