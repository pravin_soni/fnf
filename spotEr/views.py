from django.http import HttpResponse, HttpResponseRedirect 
from django.contrib.auth import login, authenticate, logout
from .forms import SignUpForm, LoginForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from spotEr.models import User, Connection
from django.db.models import Q
import xlwt
# Create your views here.


def index(request):
    """Home Page."""
    my_connections = Connection.objects.filter(Q(follower=request.user.id) | Q(following=request.user.id))
    a = []
    a.append(request.user.id)
    for o in my_connections:
        if o.follower_id not in a:
            a.append(o.follower_id)
        if o.following_id not in a:
            a.append(o.following_id)
    if request.method == 'POST':
        search_string = request.POST['search']
        all_users = User.objects.exclude(id__in=a).filter(Q(first_name__contains=search_string) | Q(last_name__contains=search_string))
    else:
        all_users = User.objects.exclude(id__in=a)
    return render(request, 'home/index.html', {'allUsers': all_users})


def sign_up(request):
    """User Registrations"""
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.info(request, "Registerd Successfully...!")
            return redirect('/spotEr')
    return render(request, 'registrations/signup.html', {})

def user_login(request):
    """User Login"""
    logout(request)
    # form = LoginForm(request.POST)
    # print(form)
    # if form.is_valid():
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        try:
            user = User.objects.get(email=email, password=password)
        except NameError:
            user = None
        if user is not None:
            login(request, user)
            messages.success(request, 'Form submission successful')
            return HttpResponseRedirect('/spotEr')
        else:
            return HttpResponseRedirect('/spotEr/login')
    # else:
    #     form = LoginForm()
    return render(request, 'registrations/login.html', {})

@login_required
def user_logout(request):
    """Logout"""
    logout(request)
    messages.info(request, "User logged out!")
    return redirect('/')

def my_connections(request):
    """my connections"""
    connections = Connection.objects.filter(Q(follower=request.user.id) | Q(following=request.user.id))
    a = []
    for o in connections:
        if o.follower_id not in a and request.user.id != o.follower_id:
            a.append(o.follower_id)
        if o.following_id not in a and request.user.id != o.following_id:
            a.append(o.following_id)

    if request.method == 'POST':
        search_string = request.POST['search']
        my_connections = User.objects.filter(Q(id__in=a) & Q(first_name__contains=search_string))
    else:
        my_connections = User.objects.filter(Q(id__in=a))
    return render(request, 'connections/connections.html', {'myConnections': my_connections})

def add_friend(request, _id):
    """add connections"""
    friend = User.objects.get(id=_id)
    adding_connection = Connection(follower=request.user, following=friend, status=1)
    adding_connection.save()
    return redirect('/')

def export_users_xls(request):
    """export connections"""
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Connections')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Username', 'First name', 'Last name', 'Email address', ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    connections = Connection.objects.filter(Q(follower=request.user.id) | Q(following=request.user.id))
    a = []
    for o in connections:
        if o.follower_id not in a and request.user.id != o.follower_id:
            a.append(o.follower_id)
        if o.following_id not in a and request.user.id != o.following_id:
            a.append(o.following_id)
    rows = User.objects.filter(id__in=a).values_list('username', 'first_name', 'last_name', 'email')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response
