from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.

class User(AbstractUser):
    pass

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username', 'password')

    password = models.CharField(max_length=128)
    username = models.CharField("person's username", max_length=150)
    first_name = models.CharField("person's first name", max_length=30)
    last_name = models.CharField("person's last name", max_length=30)
    email = models.CharField("person's email", max_length=254, unique=True)

class Connection(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower')
    following = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following')
    status = models.IntegerField()
    