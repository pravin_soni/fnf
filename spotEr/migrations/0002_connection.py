# Generated by Django 2.1.7 on 2019-06-15 05:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spotEr', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Connection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('follower_id', models.IntegerField()),
                ('following_id', models.IntegerField()),
                ('status', models.IntegerField()),
            ],
        ),
    ]
