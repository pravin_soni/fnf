from django.forms import ModelForm
from spotEr.models import User


class SignUpForm(ModelForm):
    """Signup form"""
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

class LoginForm(ModelForm):
    """Login Form"""
    class Meta:
        model = User
        fields = ['email', 'password']
    # # this function will be used for the validation 
    # def clean(self): 
    #     # data from the form is fetched using super function 
    #     super(LoginForm, self).clean()   
    #     # extract the username and text field from the data 
    #     email = self.cleaned_data.get('email') 
    #     password = self.cleaned_data.get('password') 
    #     try:
    #         user = User.objects.get(email=email, password=password)
    #     except:
    #         user = None
    #     print(user)
    #     if user is not None:
    #         self._errors['email'] = self.error_class([ 
    #             'Invalid email or password']) 
    #     # return any errors if found 
    #     return self.cleaned_data 

# class ChangePassword(ModelForm):
#     """change passsword"""
#     class Meta:
#         model = User
#         fields = ['current_password', 'new_password', 'repeat_password']
