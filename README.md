**fnf : Friends and Family**, is a simple Django project for beginners.
Here you can perform some actions like login, signUp, search friends, add friends, export in CSV.

**1** git clone https://pravin_soni@bitbucket.org/pravin_soni/fnf.git  
**2** Setup XAMPP  
**3** start MYSQL and Apache  
**4** setup you virtual environment (optional)  
**5** Open CMD and Change Directory to fnf  
**6** run command **pip install xlwt** (package for csv export)  
**7** and run command **python manage.py runserver** to run server  
**8** if all ok, open your browser and hit this url **http://127.0.0.1:8000/**  